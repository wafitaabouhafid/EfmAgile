<?php
// Database connection parameters.
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'todolist');
define('DB_HOST', '127.0.0.1');
define('DB_PORT', '3306');
// Connexion à la base de données
$conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME, DB_PORT);

// Vérifier la connexion à la base de données
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Lire la liste des tâches triées par date de création (du plus récent au plus ancien)
$result = $conn->query("SELECT * FROM todo ORDER BY created_at DESC");

$taches = array();

// Parcourir les résultats et les stocker dans la variable $taches
while ($row = $result->fetch_assoc()) {
    $taches[] = $row;
}

// Vérifier l'action reçue en POST et effectuer les opérations correspondantes
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['action'])) {
        $action = $_POST['action'];

        switch ($action) {
            case 'new':
                // Ajouter une nouvelle tâche à la table todo
                if (isset($_POST['title'])) {
                    $title = $_POST['title'];
                    $conn->query("INSERT INTO todo (title) VALUES ('$title')");
                }
                break;

            case 'delete':
                // Supprimer la tâche de la table todo
                if (isset($_POST['id'])) {
                    $id = $_POST['id'];
                    $conn->query("DELETE FROM todo WHERE id = $id");
                }
                break;

            case 'toggle':
                // Basculer la valeur du champ 'done' entre true et false pour la tâche en question
                if (isset($_POST['id'])) {
                    $id = $_POST['id'];
                    $conn->query("UPDATE todo SET done = 1 - done WHERE id = $id");
                }
                break;

            default:
                // Gérer d'autres actions si nécessaire
                break;
        }
    }
}

// Fermer la connexion à la base de données
$conn->close();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ToDo List</title>
</head>
<body>
    <h1>My ToDo List</h1>
    <!-- Your HTML content goes here -->
</body>
</html>

